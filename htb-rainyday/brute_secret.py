#!/usr/bin/env python3

import bcrypt
import string
import subprocess


secret = ""
while True:
    # generate next password
    pass_len = 71 - len(secret)
    num_santa = pass_len // 4
    num_a = pass_len % 4
    password = "🎅" * num_santa + "A" * num_a

    # get hash
    proc = subprocess.run(
        ["./cheat/hash_password.py"], input=password.encode(), stdout=subprocess.PIPE
    )
    hash = proc.stdout.split(b" ")[4].strip()

    # brute last character
    for c in string.printable[:-6]:
        print(f"\r{secret}{c}", end="")
        if bcrypt.checkpw((password + secret + c).encode(), hash):
            secret += c
            break
    # There is no more character - end loop
    else:
        break

print(f"\r{secret:50}")
